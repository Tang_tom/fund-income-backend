const mysql = require('mysql');
const session = require('express-session');
const mysqlSession = require('express-mysql-session')(session)
const db_config = require('./db_config');
// 连接数据库
const db = mysql.createConnection(db_config)
const DB_ERROR = 'sql连接错误';
const sessionStore = new mysqlSession({},db)
db.connect(err =>{
    if (err) throw err;
    console.log('数据库连接成功')
})
db.DB_ERROR = DB_ERROR;
module.exports = db;