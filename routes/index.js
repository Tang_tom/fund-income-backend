var express = require('express');
var router = express.Router();
const utils = require('./utils/utils');
// 数据库相关配置和方法
router.get('/', function (req, res, next) {
  res.end()
});
router.post('/login', function (req, res, next) {
  let mustArr = ['name', 'password'];
  let notMustArr = [];
  let numberArr = [];
  let params = utils.checkParams({
      data: req.body,
      mustArr,
      notMustArr,
      numberArr
  })
  if (params['msg']) {
      res.send({
          code: 400,
          msg: params['msg']
      })
      return
  }
  utils.setToken(params['name']).then((token) => {
      res.send({
          code: 200,
          data: {
              token,
              username:params['name'],
          }
      });
  }).catch(err => {
      res.send({
          code: 400,
          msg: err
      });
  })

});
module.exports = router;